// console.log("Happy Tuesday!")

// [SECTION] Synchronous vs Asynchronous

// Javascript, by default, is synchronous meaning it can only execute a block of code by line
// It reads from top to bottom and left to right

console.log("Hello World!");
// conosle.log("HEllo Again!");
console.log("Goodbye!")

// [SECTION] Getting All Posts
// A Fetch API allows you to asynchronously request for a resourse/data
// "Promise" is an object that represents the eventual completion (or failure) of a asynchronous request
/*
	Syntax:
		fetch('URL')
*/

// Retrieves all posts following the Rest API (retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise
// the connection or the server/url is still available
console.log(fetch("https://jsonplaceholder.typicode.com/posts")
	// The "fetch" method will return a "promise" that resolves to a "Response" object
	// The "then" method captures the "Reponse" object and returns another "promise" which will eventually be "resolved" or "rejected"
	.then(response => console.log(response.status)));


fetch("https://jsonplaceholder.typicode.com/posts")
// response -> raw data
// response.json() -> converts data
// Use the "json" method from the "Response" object to convert the data retrieved into JSON format to be used in our application
.then(response => response.json())
// json => converted data
// Print the converted JSON value from the "fetch" request
// Using multiple "then" methods creates a "promise chain"
.then(json => console.log(json));

// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for
// Creates an asynchronous function
async function fetchData() {

	// waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts")

	// Result returned by fetch is a promise
	console.log(result)
	// The returned "Response" is an object
	// We cannot access the content of the "Response" by directly accessing it's body property
	console.log(typeof result)

	// Converts the data from the "Response" object as JSON
	let json = await result.json()
	// Print out the content of the "Response" object
	console.log(json)
}

fetchData();

// [SECTION] Getting Specific Post

// retrieve a specific post following the REST API (retrieve, /posts/:id, GET)
fetch("https://jsonplaceholder.typicode.com/posts/5")
.then(response => response.json())
.then(json => console.log(json))

// [SECTION] Creating a Post
/*
	Syntax:
		fetch('URL', options)
		.then((response)=>{})
		.then((response)=>{})
*/

// create a post following the REST API (create/insert, /posts, POST)
fetch("https://jsonplaceholder.typicode.com/posts", {
	// Sets the method of the "Request" object to "POST" following REST API
	// Default method is GET
	method: "POST",
	// Sets the header data of the "Request" object to be sent to the backend
	// Specified that the content will be in a JSON structure
	headers: {
		'Content-Type': 'application/json'
	},
	// Sets the content/body data of the "Request" object to be sent to the backend
	// JSON.stringify converts the object data into a stringified JSON
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

// [SECTION] Updating a post using PUT Method

// updating a post following the REST API (update, /posts/:id, PUT)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated POst Title",
		body: "Hello Again from ID 1",
	})
})
.then(response => response.json())
.then(json => console.log(json))

// [SECTION] Updating a post using PATCH Method
// The difference between PUT and PATCH is the number of properties being changed
	// PATCH is used to update the whole object
	// PUT is used to update a single/several properties

// updating a post following the REST API (update, /posts/:id, PATCH)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected Post"
	})
})
.then(response => response.json())
.then(json => console.log(json))

// [SECTION] Deleting a Post

// deleting a post following the REST API (delete, /posts/:id, DELETE)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
})

// [SECTION] Filtering a Post
// The data can be filtered by sending the userId along with the URL
// Information sent vie the url can be done by adding the question mark symbol (?)
/*
	Syntax:
		- Individual Parameters
			'url?parameterName=value'
		- Multiple Parameters
			'url?paramA=valueA&paramB=valueB'
*/
fetch("https://jsonplaceholder.typicode.com/posts?userId=10")
.then(response => response.json())
.then(json => console.log(json))


